import logging
import os

import tomli

logger = logging.getLogger('energy_monitor')


def load_config(config_file='config.toml'):
    logger.debug(f"Attempting to load config from {config_file}")
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    file = os.path.exists(os.path.join(ROOT_DIR, config_file))
    if not file:
        logger.error("Config file not found")
    try:
        with open(os.path.join(ROOT_DIR, config_file), 'rb') as f:
            config = tomli.load(f)
    except Exception as e:
        logger.error(e)
        logger.error("here")

    return config
