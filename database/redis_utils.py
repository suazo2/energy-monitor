import json

import redis
from utils import load_config


class RedisManager:
    def __init__(self):
        self.redis_conn = None

    def connect(self):
        config = load_config()
        config = config["redis"]
        self.redis_conn = redis.Redis(
            host=config.get("host", "localhost"),
            port=config.get("port", 6379),
            decode_responses=True
        )

    # Save data to Redis
    def set_channel_data(self, channel, data):
        key = f"channel_{channel}"
        value = json.dumps(data)
        self.redis_conn.set(key, value)

    # Get data from Redis
    def get_channel_data(self, channel):
        key = f"channel_{channel}"
        value = self.redis_conn.get(key)
        if value:
            return json.loads(value)
        return None

    def disconnect(self):
        self.redis_conn.close()
